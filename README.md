# tkz-gallery

**Ti*k*Z**
> **Ti*k*Z** ist kein Zeichnenprogramm

***

Little collection of my PGF/TikZ figures.
I will do my best to keep it updated as soon as new figures are published.

## Thanks

The idea of this collection come from <https://github.com/PetarV-/TikZ/> by Petar Veličković.


## License

MIT (unless specified otherway).
