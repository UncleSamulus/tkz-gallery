# Manhattan Distance

<img src="./manhattan.svg" alt="Manhattan distance schema">

## Note

Green line is euclidian distance, others are example of Manhattan distance.

Implementation of the Wikimedia `Manhattan distance.svg` by Psychonaut <https://commons.wikimedia.org/wiki/File:Manhattan_distance.svg>.
